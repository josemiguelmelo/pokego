module.exports = {

    getDatabaseConnectionURL : function() {
        if(process.env.MONGODB_URI.length > 0)
        {
            console.log(process.env.MONGODB_URI);
            return process.env.MONGODB_URI;
        }
        var url = "";
        url= url + process.env.DATABASE_TYPE + "://";
        if(process.env.DATABASE_USERNAME != "" && process.env.DATABASE_PASSWORD != "")
        {
            url = url + process.env.DATABASE_USERNAME + ":" + process.env.DATABASE_PASSWORD + "@";
        }
        url = url + process.env.DATABASE_HOST + ":" + process.env.DATABASE_PORT + "/" + process.env.DATABASE_DB;
        return url;
    }

};