// Search pokemons around location
db.pokemons.aggregate([
  {
    $unwind: "$location"
  },
  {
    $project: {
         distance: {
            $let: {
               vars: {
                  lati: { $subtract: [ "$location.lat", 12 ] },
                  longi: { $subtract: [ "$location.long", 12 ] }

               },
               in: { $sqrt: { $sum: [ {$multiply : [ "$$lati", "$$lati" ]}, {$multiply : [ "$$longi", "$$longi" ]}  ] } }
            }
         },
         name : 1,
         id : 1,
         types : 1
      }
  },
  {
    $match : {
        name : 'Bulbasaur'
    }
  }
])