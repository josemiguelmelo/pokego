var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var flash    = require('connect-flash');
var session  = require('express-session');
var tls = require('tls');
var fs = require('fs');

require('./app/config/passport')(passport); // pass passport for configuration

var app = express();


if (app.get('env') === 'development') {
  require('dotenv').config();
}


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use(session({
  secret: process.env.APP_SECRET
})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session


require('./routes/index')(app, passport);
require('./routes/authentication')(app, passport);
var mobileRoutes = require('./routes/mobile')(app, passport);


var pokemonsAPI = require('./routes/api/pokemons')();
var usersAPI = require('./routes/api/users')();


app.use('/api', pokemonsAPI );
app.use('/api', usersAPI );

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}


// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


/*

tls.createServer(options, app).listen(process.env.PORT || 3100, function () {
  console.log('Started!');
});
*/
app.listen(process.env.PORT || 3200, function () {
  console.log('Example app listening on port 3100!');
});

module.exports = app;
