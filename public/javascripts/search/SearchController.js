
pokegoApp.controller("SearchController", function($scope, XMLParser, CookiesService, HTTPService, MapService) {
    $scope.searchInputString = "";
    $scope.showingAllResults = true;
    $scope.pokemon = null;
    $scope.searchingEnabled = false;

    var map;
    var markers;
    $scope.coordinates = {
        latitude : 0,
        longitude: 0
    };

    $scope.myPosition = {
        latitude : 0,
        longitude: 0
    };

    function GetRequestParams(){

        var bounds = map.getBounds();
        var distance = 0.07;
        if(bounds !== undefined)
            distance = Math.abs(bounds.b.b - bounds.b.f);

        return {
            'lat' : $scope.coordinates.latitude,
            'long' : $scope.coordinates.longitude,
            'distance' : distance
        };
    }

    function GetRequestURL(){
        if ( $scope.showingAllResults)
        {
            return '/api/pokemon/location';
        }else{
            return '/api/pokemon/location/' + $scope.pokemon.id;
        }
    }

    function GetPokemonsRequest(url, params)
    {
        MapService.removeAllMarker(markers);
        markers = [];

        var myLatLng = {lat: $scope.myPosition.latitude, lng: $scope.myPosition.longitude};
        var mark = new google.maps.Marker({
            position: myLatLng,
            map : map
        });

        var response = HTTPService.getWithParams(url, params);
        response.then(function(data){
            var pokemonsInArea = data.data;

            for(var i = 0; i < pokemonsInArea.length; i++)
            {
                var loca = {lat: pokemonsInArea[i].location.lat, lng: pokemonsInArea[i].location.long};

                var iconD = {
                    url: "/api/pokemon/image?url=" + pokemonsInArea[i].image, // url
                    scaledSize: new google.maps.Size(map.zoom * 3, map.zoom * 3), // scaled size
                    origin: new google.maps.Point(0,0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                };

                var marker = new google.maps.Marker({
                    position: loca,
                    icon : iconD,
                    map: map
                });

                markers.push(marker);
            }
        });
    }



    MapService.initMap(map, function(mapReceived, markersRece){
        map = mapReceived;
        map.maxZoom = 17;
        markers = markersRece;
        if(markers.length > 0) {
            $scope.myPosition.latitude  = markers[ 0 ].position.lat();
            $scope.myPosition.longitude = markers[ 0 ].position.lng();

            $scope.coordinates.latitude  = markers[ 0 ].position.lat();
            $scope.coordinates.longitude = markers[ 0 ].position.lng();
        }
        google.maps.event.addListener(map, 'center_changed', function(){

            if($scope.coordinates.latitude - map.getCenter().lat() > 0.001 ||
                $scope.coordinates.longitude - map.getCenter().lng() > 0.001)
            {
                $scope.coordinates.latitude = map.getCenter().lat();
                $scope.coordinates.longitude = map.getCenter().lng();

                GetPokemonsRequest(GetRequestURL() , GetRequestParams());
            }else{
                $scope.coordinates.latitude = map.getCenter().lat();
                $scope.coordinates.longitude = map.getCenter().lng();
            }

        });

        google.maps.event.addListener(map, 'zoom_changed', function() {
            $scope.coordinates.latitude = map.getCenter().lat();
            $scope.coordinates.longitude = map.getCenter().lng();

            GetPokemonsRequest(GetRequestURL() , GetRequestParams());

        });


        GetPokemonsRequest(GetRequestURL(), GetRequestParams());


    });

    $scope.dateToString = function (date)
    {
        var dateObj = date.split('T');

        var hoursObj = dateObj[1].split('.');

        return dateObj[0] + " " + hoursObj[0];
    };

    $scope.searchValueChanged = function(){
        if($scope.searchInputString == ""){
            $scope.searchingEnabled = false;
            return;
        }
        $scope.searchingEnabled = true;
        $scope.showingAllResults = true;
        $scope.pokemon = null;
        var response = HTTPService.getWithParams("/api/pokemon/search", { q : $scope.searchInputString});
        response.then(function(data){
            $scope.searchResults = data.data;
        });
    };

    $scope.openPokemon = function(pokemon){
        $scope.pokemon = pokemon;
        $scope.showingAllResults = false;

        GetPokemonsRequest(GetRequestURL(), GetRequestParams());
    };


    $scope.closePokemon = function(){
        $scope.pokemon = null;
        $scope.showingAllResults = true;

        GetPokemonsRequest(GetRequestURL(), GetRequestParams());
    };

    $scope.showPokemon = function(pokemon, location){
        var center = new google.maps.LatLng(location.lat, location.long);

        $scope.coordinates.latitude = location.lat;
        $scope.coordinates.longitude = location.long;

        GetPokemonsRequest(GetRequestURL(), GetRequestParams());

        // using global variable:
        map.setZoom(14);
        map.panTo(center);
    };

});
