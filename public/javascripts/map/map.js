var map;
var currentPosition;

function initMap() {
    getLocation();
}
google.maps.event.addDomListener(window, 'load', initMap);


function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, function(){
            console.log("Geolocation could not be calculated.");
        });
    } else {
        console.log("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
    currentPosition = position;

    var myLatLng = {lat: position.coords.latitude, lng: position.coords.longitude};

    map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
        zoom: 15
    });

    var iconImage = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: "I'm here!!",
        icon: iconImage
    });
}


// Adds a marker to the map.
function addMarker(location, map) {
    // Add the marker at the clicked location, and add the next-available label
    // from the array of alphabetical characters.
    var marker = new google.maps.Marker({
        position: location,
        label: labels[labelIndex++ % labels.length],
        map: map
    });
}
