

pokegoApp.service('MapService', ['$http', function($http){

    this.initMap = function(map , callback){
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var myLatLng = {lat: position.coords.latitude, lng: position.coords.longitude};

                map = new google.maps.Map(document.getElementById('map'), {
                    center: myLatLng,
                    zoom: 15,
                    streetViewControl: false,
                    styles : [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
                });

                var mark = new google.maps.Marker({
                    position: myLatLng,
                    map : map
                });
                var markers = [];
                markers.push(mark);
                callback(map, markers);
            }, function(){
                var myLatLng = {lat: 0, lng: 0};

                map = new google.maps.Map(document.getElementById('map'), {
                    center: myLatLng,
                    zoom: 5,
                    streetViewControl: false,
                    styles : [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
                });

                var markers = [];
                callback(map, markers);
                console.log( "Geolocation could not be calculated.");
            });
        } else {
            var myLatLng = {lat: 0, lng: 0};

            map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                zoom: 5,
                streetViewControl: false,
                styles : [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
            });

            var markers = [];
            callback(map, markers);


            console.log( "Geolocation is not supported by this browser.");
        }
    };

    this.onMapClickListener = function(map, callback){
        google.maps.event.addListener(map, 'mousedown', function(event) {
            callback(event);
        });
    };

    this.removeAllMarker = function(markers)
    {
        for(var i = 0 ; i < markers.length; i++)
        {
            markers[i].setMap(null);
        }
    };


// Adds a marker to the map.
    this.addMarker = function(location, label, map, markers, icon) {
        // Add the marker at the clicked location, and add the next-available label
        // from the array of alphabetical characters.
        var marker;
        if(icon === undefined)
        {
            if(label == null)
            {
                marker = new google.maps.Marker({
                    position: location,
                    map : map,
                    optimized: false
                });
            }else{
                marker = new google.maps.Marker({
                    position: location,
                    label: label,
                    map: map,
                    optimized: false
                });
            }
        }else{
            console.log(map.zoom);
            var iconD = {
                url: icon, // url
                scaledSize: new google.maps.Size(34, 34), // scaled size
                origin: new google.maps.Point(0,0), // origin
                anchor: new google.maps.Point(0, 0) // anchor
            };
            if(label == null)
            {
                marker = new google.maps.Marker({
                    position: location,
                    icon : iconD,
                    map : map,
                    optimized: false
                });
            }else{
                marker = new google.maps.Marker({
                    position: location,
                    label: label,
                    icon : iconD,
                    map: map,
                    optimized: false
                });
            }
        }

        markers.push(marker);

        return markers;
    }

}]);