
pokegoApp.controller("CreateEventController", function($scope, $sce, XMLParser, CookiesService, HTTPService, MapService) {

    $scope.user = $("input[name='user']").val();

    $scope.error = {
        success : null,
        msg : ""
    };

    $scope.event = {
        name : "",
        description : "",
        location: {
            latitude : null,
            longitude : null,
            country : ""
        },
        date : ""
    };

    var map;
    var markers = [];
    console.log("here");

    MapService.initMap(map, function(mapReceived, markersRece){
        console.log("here2");
        map = mapReceived;
        markers = markersRece;
        if(markers.length > 0)
        {
            $scope.event.location.latitude = markers[0].position.lat();
            $scope.event.location.longitude = markers[0].position.lng();
            $scope.$apply();
        }


        MapService.onMapClickListener(map, function(event){
            MapService.removeAllMarker(markers);
            markers = [];
            MapService.addMarker(event.latLng, null, map, markers);

            var lati = event.latLng.lat();
            var longi = event.latLng.lng();

            $scope.event.location.latitude = parseFloat(lati);
            $scope.event.location.longitude = parseFloat(longi);

            $scope.$apply();
        });
    });

    function inputHasErrors(){
        var hasErrors = false;
        if($scope.event.name == "")
        {
            $scope.error.msg = $scope.error.msg + "Name can not be empty.<br>";
            hasErrors = true;
        }
        if($scope.event.description == "")
        {
            $scope.error.msg = $scope.error.msg + "Description can not be empty.<br>";
            hasErrors = true;
        }
        if($scope.event.date == "" || $scope.event.date == null)
        {
            $scope.error.msg = $scope.error.msg + "Date can not be empty.<br>";
            hasErrors = true;
        }
        if($scope.event.location.latitude == "" || $scope.event.location.latitude == null)
        {
            $scope.error.msg = $scope.error.msg + "Location on map must be set.<br>";
            hasErrors = true;
        }
        if($scope.event.location.longitude == "" || $scope.event.location.longitude == null)
        {
            $scope.error.msg = $scope.error.msg + "Location on map must be set.<br>";
            hasErrors = true;
        }
        if($scope.event.location.country == "")
        {
            $scope.error.msg = $scope.error.msg + "Country/City can not be empty.<br>";
            hasErrors = true;
        }
        return hasErrors;

    }
    $scope.createEvent = function()
    {
        console.log($scope.error);
        if(inputHasErrors()){
            $scope.error.success = false;
            $scope.error.msg = $sce.trustAsHtml($scope.error.msg);
        }else{
            var params = {
                username : $scope.user,
                title : $scope.event.name,
                description : $scope.event.description,
                date : $scope.event.date,
                latitude : $scope.event.location.latitude,
                longitude : $scope.event.location.longitude,
                country : $scope.event.location.country
            };

            var response = HTTPService.post('/api/users/events/store', params);
            response.then(function(data){
                $scope.error = data.data;
                $scope.error.msg = $sce.trustAsHtml($scope.error.msg);
            });
        }

    };



});
