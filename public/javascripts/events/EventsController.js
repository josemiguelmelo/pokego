
pokegoApp.controller("EventsController", function($scope, XMLParser, CookiesService, HTTPService, MapService) {

    $scope.events = [];

    $scope.user = $("#user").data("user");

    $scope.search = {
        name: "",
        location : "",
        from_date : "",
        to_date : ""
    };

    $scope.searchEvent = function(){
        var params = {
            name : $scope.search.name,
            location : $scope.search.location,
            from_date : $scope.search.from_date,
            to_date : $scope.search.to_date
        };
        var searchResponse = HTTPService.getWithParams('/api/users/events', params);
        searchResponse.then(function(data){
            $scope.events = data.data;
            console.log($scope.events);
        });

    };
    var response = HTTPService.get('/api/users/events');
    response.then(function(data){
        $scope.events = data.data;
        console.log($scope.events);
    });

    $scope.alreadyLikedEvent = function(post)
    {
        if($scope.user === 'undefined')
        {
            return false;
        }

        for(var i = 0; i < post.local.events.likes.length; i++)
        {
            if(post.local.events.likes[i].username == $scope.user.local.username)
            {
                return true;
            }
        }
        return false;
    };

    $scope.likeEvent = function(post)
    {
        var params = {
            username : post.local.username,
            post_id : post.local.events._id,
            like_username : $scope.user.local.username
        };
        var response = HTTPService.post('/api/users/events/likes/store', params);
        response.then(function(data){
            console.log(data);
            if(data.data.success == true)
            {
                post.local.events.likes.push({ username : $scope.user.local.username});
                $scope.$apply();
            }
        });


    }
});