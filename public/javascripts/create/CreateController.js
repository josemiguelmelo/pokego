
pokegoApp.controller("CreateController", function($scope, XMLParser, CookiesService, HTTPService, MapService) {
    $scope.pokemons;
    $scope.poke;
    $scope.user = $("input[name='user']").val();

    $scope.error = {
        success : null
    };
    var map;

    var markers = [];
    $scope.coordinates = {
        latitude : 0,
        longitude: 0
    };

    var response = HTTPService.getWithParams("/api/pokemon/search", { q : ""});
    response.then(function(data){

        MapService.initMap(map, function(mapReceived, markersRece){
            map = mapReceived;
            markers = markersRece;

            $scope.coordinates.latitude = markers[0].position.lat();
            $scope.coordinates.longitude = markers[0].position.lng();
            $scope.$apply();

            MapService.onMapClickListener(map, function(event){
                MapService.removeAllMarker(markers);
                markers = [];
                MapService.addMarker(event.latLng, null, map, markers);

                var lati = event.latLng.lat();
                var longi = event.latLng.lng();

                $scope.coordinates.latitude = parseFloat(lati);
                $scope.coordinates.longitude = parseFloat(longi);

                $scope.$apply();
            });
        });

        $scope.pokemons = data.data;
    });


    $scope.savePokemon = function()
    {
        var params = {
            latitude : $scope.coordinates.latitude,
            longitude : $scope.coordinates.longitude,
            user : $scope.user,
            id : $scope.poke
        };

        var response = HTTPService.post('/api/pokemon/location/store', params);
        response.then(function(data){
            $scope.error = data.data;
            $scope.apply();
        });
    };



});