var pokegoApp = angular.module('pokegoApp', ['ngCookies', 'ngRoute']);
pokegoApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});


pokegoApp.service('CookiesService', ['$cookies', function($cookies){

    this.putObject = function(name, object){
        $cookies.putObject(name, object,{
            path: "/"
        });
    };

    this.getObject = function(name){
        return $cookies.get(name);
    }
}]);

pokegoApp.filter('reverse', function() {
    return function(items) {
        return items.slice().reverse();
    };
});

pokegoApp.service('HTTPService', ['$http', function($http){

    this.post = function(url, params){
        return $http.post(
            url,
            params).
        then(function(response){
            return response;
        });
    };

    this.get = function(url){
        return $http.get(url).
        then(function successCallback(response){
            return response;
        }, function errorCallback(response){
            return false;
        });
    };


    this.getWithParams = function(url, params){
        return $http({method: "get", url:url, params: params}).
        then(function(response){
            return response;
        });
    };
}]);



pokegoApp.service('XMLParser', [function(){

    this.ParseXML = function(val)
    {
        if (window.DOMParser)
        {
            parser=new DOMParser();
            xmlDoc=parser.parseFromString(val,"text/xml");
        }
        else // Internet Explorer
        {
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM"); xmlDoc.loadXML(val);
        }
        return xmlDoc ;
    };
}]);