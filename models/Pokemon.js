// grab the things we need
var mongoose = require('mongoose');
var connectionURL = require('../helper/database').getDatabaseConnectionURL();
mongoose.connect(connectionURL);

var Schema = mongoose.Schema;

// create a schema
var pokemonSchema = new Schema({
    id : Number,
    name: String,
    types: [String],
    image: String,
    location : [
        {
            lat: Number,
            long: Number,
            date : Date,
            user : String
        }
    ]
});

// the schema is useless so far
// we need to create a model using it
var Pokemon = mongoose.model('Pokemon', pokemonSchema);

// make this available to our users in our Node applications
module.exports = Pokemon;