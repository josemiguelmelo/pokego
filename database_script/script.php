<?php

include 'simplehtmldom_1_5/simple_html_dom.php';


class Pokemon{
  public $id;
  public $name;
  public $types;
  public $image;

  public function __construct($name, $types, $id){
    $this->id = $id;
    $this->name = $name;
    $this->types = $types;
  }

  public function typesToJSON()
  {
    $string = '[ ';
    $index = 0;
    foreach($this->types as $type)
    {
      if($index != 0)
        $string = $string . " , ";
      $string = $string . '"' . $type . '"';
      $index++;
    }

    $string = $string. ']';
    return $string;
  }
  public function toJSON(){
    return '{ id : '. $this->id . ' , name : "'. $this->name . '", image : "' . $this->name . ".png" .
      '", types : ' .$this->typesToJSON() . ' , location: []' . " }";
  }
}




function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}


$pokemons = [];
$id = 1;

/**
* Write file with all the queries to populate mongodb pokemons table
*/
function writePokemonsToFile($pokemonsList){
  $myfile = fopen("pokemons.txt", "w") or die("Unable to open file!");
  foreach ($pokemonsList as $pokemon) {
    fwrite($myfile, "db.pokemons.insert(" . $pokemon->toJSON() . ")\n");
  }
  fclose($myfile);

}

/**
* Create all pokemon images.
*/
function writeImagesToFile($pokemonsList){
  foreach ($pokemonsList as $pokemon) {
    $ch = curl_init($pokemon->image);
    $fp = fopen("pokemons/" . $pokemon->name . '.png' , 'wb');
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
  }
  fclose($myfile);

}


/**
* Get list of pokemons info from pokemons.html
*/
function getPokemons()
{
  $pokemonsList = [];
  $html = file_get_html('pokemons.html');
  $i = 0;
  foreach($html->find('tr') as $element)
  {
    $name = "";
    $types = [];
    $id = -1;
    foreach($element->find('td') as $node){

            $node = str_replace("<td>", "",$node);
            $node = str_replace("</td>", "",$node);
            $node = str_replace("<br>", "",$node);

            $evolvesArray = explode('<strong>Evolves into</strong>: ', $node);
            $array = explode('<strong>', $evolvesArray[0]);
            if(count($array) > 1){
              echo $node. "\n";

              $id = str_replace("#", "", $array[0]);
              $id = str_replace("-", "", $id);
              $id = str_replace(" ", "", $id);

              $name = str_replace(" </strong>", "", $array[1]);
              $typesString = str_replace("Type</strong>: ", "", $array[2]);
              $typesString = str_replace(" ", "", $typesString);
              $types = explode("/", $typesString);
              echo "id = " . $id . "\n";
              echo "name = " . $name . "\n";
              echo "types = " ; print_r($types); echo "\n\n";

              $pokemonsList[] = new Pokemon($name, $types, $id);
            }


    }

  }
  return $pokemonsList;
}


/**
* Get images to each pokemon from pokemons.html
*/
function getImages($pokemonsList)
{
  $html = file_get_html('pokemons.html');
  $i = 0;
  foreach($html->find('img') as $element)
  {
    $pokemonsList[$i]->image = $element->src;
    echo $element->src . "\n"; $i++;
  }
  return $pokemonsList;
}


$pokemons = getPokemons();
$pokemons = getImages($pokemons);
writePokemonsToFile($pokemons);
