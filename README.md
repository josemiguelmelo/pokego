# PokeGlobe

Share with Pokemon Go world where you found your pokemons!  
Organize events for Pokemon Go community!

## Technologies

PokeGlobe was built on top of Express, a NodeJS framework - http://expressjs.com/.  
As template engine it was used jade - https://pugjs.org/api/getting-started.html.


## How to install

1) Clone or download the project. 

2) Install all dependencies using:

```
$ npm install
```

3) Add the following environment variables or create a .env file with them:  

```
DATABASE_TYPE='mongodb'
DATABASE_HOST=''
DATABASE_PORT=''
DATABASE_DB=''
DATABASE_USERNAME=''
DATABASE_PASSWORD=''
MONGODB_URI=''
APP_SECRET=''
PORT=''
MAILGUN_API_KEY=''
MAILGUN_DOMAIN=''
```

> Note: PokeGlobe uses MailGun, so it is needed to create an application on that platform.


4) Generate the file with the queries to populate the pokemons table, which is needed for the application to work correctly.<br>To generate this file, run the script that is located under **database_script/**. To run this script go to the specified folder and run the following command:

```
$ php script.php
```

After running this command a .txt file was created with all the mongo queries needed to populate the Pokemons table.

> Note: Don't forget to create the pokemons table before running the queries. Name the table as **pokemons**


5) To run all the queries, you can use MongoIDE tool. [You can find it here](https://bitbucket.org/josemiguelmelo/mongo-ide)


## Usage

#### Start application
After installing all dependencies, initialize application using:
````
$ npm start
```

Or, if you have nodemon installed, using:
```
$ nodemon ./app.js
```

Access localhost:port_number on your browser, where port_number is the port the server is listening to.


## Deploy to heroku

First create the application on Heroku platform.  
After creating the application on Heroku, add heroku remote to the base folder of the project with the following command:

```
$ heroku git:remote -a {app_name}
```
where {app_name} is the application name on heroku.

Finally, add push the project to heroku using the following commands:
```
$ git add .
$ git commit -am "commit message"
$ git push heroku master
```
