var express = require('express');
var User = require('../../models/User');
var Array = require('../../helper/Array');

module.exports = function(){
    'use strict';
    var api = express.Router();

    api.get('/users/all', function(req, res, next){
        User.find({}, function(err, users){
            res.send(users);
        })
    });

    api.get('/users/search', function (req, res, next) {
        if(req.body.email !== undefined)
        {
            User.find({'local.email' : req.param("email")}, function(err, users){
                res.send(users);
            });
        }else {
            User.find({'local.username' : req.param("username")}, function(err, users){
                res.send(users);
            });
        }
    });
    api.get('/users/events', function (req, res, next) {
        var name = req.param("name");
        var location = req.param("location");
        var fromDate = req.param("from_date");
        var toDate = req.param("to_date");

        var matchArray = {
            'local.events' : { $exists : true }
        };
        if(name != "")
            matchArray["local.events.title"] = new RegExp(".*" + name + ".*", "i");
        if(location != "")
            matchArray["local.events.location.country"] = new RegExp(".*" + location + ".*", "i");
        if(fromDate != "" && fromDate !== undefined)
            matchArray["local.events.date"] = { $gte : new Date(fromDate)};
        if(toDate != "" && toDate !== undefined)
            matchArray["local.events.date"] = { $lte : new Date(toDate)};

        var aggregateArray = [
            { $unwind : "$local.events" },
            {
                $project: {
                    'local.events' : 1,
                    'local.username' : 1,
                    'local.email' : 1
                }
            },
            {
                $match : matchArray
            },
            {
                $sort : { 'local.events.date' : 1}
            }];

        User.aggregate( aggregateArray
            , function(err, posts){
                res.send(posts);
            });
    });

    api.post('/users/events/likes/store', function(req, res, next) {
        User.findOne({'local.username' : req.body.username}, function(err, user){
            if(user.local.events === undefined)
            {
                res.send({success: false, msg: "Like could not be added."});
                return;
            }

            for( var i = 0; i < user.local.events.length; i++)
            {
                if(user.local.events[i]._id == req.body.post_id)
                {
                    if(user.local.events[i].likes === undefined)
                    {
                        user.local.events[i].likes = [];
                    }
                    if(!Array.containsUsername(user.local.events[i].likes, "username", req.body.like_username))
                    {
                        user.local.events[i].likes.push({
                            username : req.body.like_username
                        });
                        user.save(function(err) {
                            if (err){
                                res.send({success: false, msg: "Like could not be added."});
                                return;
                            }


                            res.send({success: true, msg: "Like added successfully."});
                        });
                    } else{
                        res.send({success: false, msg: "Like could not be added"});
                        return;
                    }

                }
            }

        });
    });
    api.post('/users/events/store', function(req, res, next) {

        User.findOne({'local.username' : req.body.username}, function(err, user){
            if(user.local.events === undefined)
            {
                user.local.events = [];
            }

            user.local.events.push({
                title : req.body.title,
                description : req.body.description,
                date : new Date(req.body.date),
                location : {
                    lat : req.body.latitude,
                    long : req.body.longitude,
                    country : req.body.country
                },
                likes : []
            });

            user.save(function(err) {
                if (err) throw err;

                res.send({success: true, msg: "Event created successfully."});
            })
        });
    });


    return api;
};
