var express = require('express');
var Pokemon = require('../../models/Pokemon');
var http = require("http");

module.exports = function(){
    'use strict';
    var api = express.Router();

    api.get('/pokemon/all', function(req, res, next){
        Pokemon.find({}, function(err, pokemons){
            res.send(pokemons);
        })
    });

    api.get('/pokemon/search', function (req, res, next) {
        Pokemon.find({name: new RegExp(".*" + req.param("q") + ".*", "i")}, function(err, pokemons){
            res.send(pokemons);
        });
    });

    api.get('/pokemon/find/:id', function(req, res, next) {
        Pokemon.find({id: req.param("id")}, function(err, pokemons){
            res.send(pokemons);
        });
    });


    api.get('/pokemon/location/:id',  function(req, res, next) {
        var latReq = req.param("lat");
        var longReq = req.param("long");
        var distance = req.param("distance");

        Pokemon.aggregate(
            [{
                $unwind: "$location"
            },
                {
                    $project: {
                        distance: {
                            $let: {
                                vars: {
                                    lati: { $subtract: [ "$location.lat", parseFloat(latReq) ] },
                                    longi: { $subtract: [ "$location.long", parseFloat(longReq) ] }

                                },
                                in: { $sqrt: { $sum: [ {$multiply : [ "$$lati", "$$lati" ]}, {$multiply : [ "$$longi", "$$longi" ]}  ] } }
                            }
                        },
                        location : 1,
                        name : 1,
                        id : 1,
                        types : 1,
                        image : 1
                    }
                },
                {
                    $match : {
                        id : parseInt(req.param("id")),
                        distance : { $lt : parseFloat(distance)  }
                    }
                }], function(err, pokemons){
                pokemons = shuffle(pokemons);
                res.send(pokemons.slice(0, 15));
            });
    });


    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    api.get('/pokemon/location',  function(req, res, next) {
        var latReq = req.param("lat");
        var longReq = req.param("long");
        var distance = req.param("distance");

        Pokemon.aggregate(
            [
            {
                $unwind: "$location"
            },
            {
                $project: {
                    distance: {
                        $let: {
                            vars: {
                                lati: { $subtract: [ "$location.lat", parseFloat(latReq) ] },
                                longi: { $subtract: [ "$location.long", parseFloat(longReq) ] }

                            },
                            in: { $sqrt: { $sum: [ {$multiply : [ "$$lati", "$$lati" ]}, {$multiply : [ "$$longi", "$$longi" ]}  ] } }
                        }
                    },
                    location : 1,
                    name : 1,
                    id : 1,
                    types : 1,
                    image : 1
                }
            },
            {
                $match : {
                    distance : { $lt : parseFloat(distance) }
                }
            }], function(err, pokemons){
                pokemons = shuffle(pokemons);
             res.send(pokemons.slice(0, 15));
        });
    });


    api.post('/pokemon/location/store', function(req, res, next) {
        if(req.body.id === undefined)
        {
            res.send({success: false, msg: "Please specify a pokemon."});
            return;
        }

        Pokemon.find({ id: parseInt(req.body.id) }, function(err, pokemon){
            if (err) throw err;

            if(pokemon === undefined)
            {
                res.send({success: false, msg: "Pokemon not found."});
                return;
            }

            // change the users location
            for(var i = 0; i < pokemon[0].location.length; i++)
            {
                var a = pokemon[0].location[i].lat - parseFloat(req.param("latitude"));
                var b = pokemon[0].location[i].long - parseFloat(req.param("longitude"));

                var c = Math.sqrt( a*a + b*b );

                if(c < 0.001)
                {
                    res.send({success : true, msg : "New location added successfully."});
                    return;
                }
            }

            pokemon[0].location.push({lat: req.param("latitude"), long: req.param("longitude"), date: new Date(), user: req.param("user")});

            // save the user

            pokemon[0].save(function(err) {
                if (err) throw err;

                res.send({success: true, msg: "New location added successfully."});
            });

        });
    });


    api.get('/pokemon/image', function(req, res, next){
        var imageUrl = req.param('url');
        res.sendFile(process.cwd() + '/images/pokemons/' + imageUrl);
    });



    return api;
};
