var Login = require('../helper/passport');
var User = require('../models/User');

var api_key = process.env.MAILGUN_API_KEY;
var domain = process.env.MAILGUN_DOMAIN;
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});


module.exports = function(app, passport) {
    app.get('/login', function (req, res) {

        // render the page and pass in any flash data if it exists
        res.render('login', {message : req.flash('loginMessage')});
    });

    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

// process the login form
// app.post('/login', do all our passport stuff here);

// =====================================
// SIGNUP ==============================
// =====================================
// show the signup form
    app.get('/signup', function (req, res) {

        // render the page and pass in any flash data if it exists
        res.render('signup', {message : req.flash('signupMessage')});
    });

// process the signup form
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/signup', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });


    app.get('/forgot_password', function(req, res){
        res.render('forgot_password', {errorMessage : req.flash('errorMessage'), confirmationMessage : req.flash('confirmationMessage')});
    });

    app.post('/recover_password', function(req, res){
        var email = req.param("email");

        User.findOne({'local.email' : email}, function(err, user){
            if(user == null)
            {
                req.flash('errorMessage', 'Email could not be found. Try again.');
                res.redirect('/forgot_password');
                return;
            }
            var newPassword = Math.random().toString(36).slice(2);

            user.local.password = user.generateHash(newPassword);

            user.save(function(err) {
                if (err)
                    throw err;


                var data = {
                    from: 'PokeGo <support@pokego.com>',
                    to: user.local.email,
                    subject: 'Recover Password',
                    text: 'Your password has been reset successfully. Your new password is: ' + newPassword
                };


                mailgun.messages().send(data, function (error, body) {
                    req.flash('confirmationMessage', 'An email with a new password has been sent to this email.');
                    res.redirect('/forgot_password');
                });
            });


        });
    });

    app.get('login_error', function (req, res, next) {
        res.render('login_error');
    });
};