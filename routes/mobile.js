var Login = require('../helper/passport');


module.exports = function(app, passport){

    app.get('/mobile/', function (req, res, next) {
        if(req.user !== undefined)
        {
            res.render('mobile_app/index', { title: 'Express', user: req.user });
        }else{
            res.render('mobile_app/index', { title: 'Express'});
        }
    });

    app.get('/mobile/new', function(req, res, next) {
        if(req.user !== undefined)
        {
            res.render('mobile_app/create', { title: 'Express', user: req.user });
        }else{
            res.render('mobile_app/create', { title: 'Express'});
        }
    });

    app.get('/mobile/events', Login.isLoggedIn, function(req, res, next) {
        res.render('mobile_app/events/show/events', {user: req.user});
    });

    app.get('/mobile/events/create', Login.isLoggedIn, function(req, res, next) {
        res.render('mobile_app/events/create/create', {user: req.user});
    });



    app.get('/mobile/profile', Login.isLoggedIn, function(req, res, next) {
        res.render('mobile_app/profile', { title: 'Express', user: req.user , errorMessage : req.flash('errorMessage'), confirmationMessage : req.flash('confirmationMessage')});
    });

    app.get('/mobile/login', function (req, res) {

        // render the page and pass in any flash data if it exists
        res.render('mobile_app/login', {message : req.flash('loginMessage')});
    });
    app.get('/mobile/signup', function (req, res) {

        // render the page and pass in any flash data if it exists
        res.render('mobile_app/signup', {message : req.flash('signupMessage')});
    });

    app.get('/mobile/logout', function (req, res) {
        req.logout();
        res.redirect('/mobile/');
    });


    app.get('/mobile/forgot_password', function(req, res){
        res.render('mobile_app/forgot_password', {errorMessage : req.flash('errorMessage'), confirmationMessage : req.flash('confirmationMessage')});
    });


    app.post('/mobile/login', passport.authenticate('local-login', {
        successRedirect : '/mobile/', // redirect to the secure profile section
        failureRedirect : '/mobile/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));


// process the signup form
    app.post('/mobile/signup', passport.authenticate('local-signup', {
        successRedirect : '/mobile/', // redirect to the secure profile section
        failureRedirect : '/mobile/signup', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));


};