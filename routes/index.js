var Login = require('../helper/passport');

var api_key = process.env.MAILGUN_API_KEY;
var domain = process.env.MAILGUN_DOMAIN;
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

module.exports = function(app, passport){
    app.get('/', function (req, res,next) {
        if(req.user !== undefined)
        {
            res.render('index', { title: 'Express', user: req.user });
        }else{
            res.render('index', { title: 'Express'});
        }
    });

    app.get('/events', Login.isLoggedIn, function(req, res, next) {
        res.render('events/show/events', {user: req.user});
    });

    app.get('/events/create', Login.isLoggedIn, function(req, res, next) {
        res.render('events/create/create', {user: req.user});
    });

    app.get('/new', function(req, res, next) {
        if(req.user !== undefined)
        {
            res.render('create', { title: 'Express', user: req.user });
        }else{
            res.render('create', { title: 'Express'});
        }
    });


    app.get('/profile', Login.isLoggedIn, function(req, res, next) {
        res.render('profile', { title: 'Express', user: req.user , errorMessage : req.flash('errorMessage'), confirmationMessage : req.flash('confirmationMessage')});
    });


    app.post('/profile', Login.isLoggedIn, function(req, res, next) {
        var username = req.param("username");
        var email = req.param("email");
        var oldPassword = req.param("old_password");
        var newPassword = req.param("new_password");

        var user = req.user;

        if(!user.validPassword(oldPassword))
        {
            req.flash('errorMessage', 'Old password not valid.');
            res.redirect('/forgot_password');
            return;
        }

        user.local.username = username;
        user.local.email = email;
        if(newPassword != "")
            user.local.password = user.generateHash(newPassword);

        user.save(function(err) {
            if (err)
                throw err;

            var data = {
                from: 'PokeGo <support@pokego.com>',
                to: user.local.email,
                subject: 'Account info updated',
                text: 'Your account info was updated. If this was not you, contact us.'
            };

            mailgun.messages().send(data, function (error, body) {
                req.flash('confirmationMessage', 'Account information updated successfully.');
                res.redirect('/profile');
            });
        });

    });
};
